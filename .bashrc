# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth
# append to the history file, don't overwrite it
shopt -s histappend
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000 # Record last 10,000 commands
HISTFILESIZE=10000 # Record last 10,000 commands per session
# set date time in history
export HISTTIMEFORMAT="%Y/%m/%d_%T : "
export HISTCONTROL=ignoreboth
export HISTIGNORE='history*'
export PROMPT_COMMAND='history -a;echo -en "\e]2;";history 1|sed "s/^[ \t]*[0-9]\{1,\}  /Terminal@/g";echo -en "\e\\";'

# https://www.gnu.org/software/bash/manual/html_node/The-Shopt-Builtin.html
shopt -s autocd
shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s expand_aliases
shopt -s globstar

# custom functions
function gitIsGitRepository()
{
    output=
    gitTopLevel=$(git rev-parse --show-toplevel 2>/dev/null)
    if [[ $gitTopLevel != '' ]]; then
        if [[ $gitTopLevel == $(pwd) ]]; then
            #sgr0 reset color too
            output="✔ on $boldOn$(gitCurrentBranch)$boldOff$purple '$boldOn$(gitLastCommit)$boldOff$purple'"
        else
            output="repository on $gitTopLevel"
        fi
        output="[Git $output]"
    fi
    echo "$output";
}
function gitCurrentBranch()
{
    # verify and quiet flags prevent fatal error on freshly created repository
    # fatal: ambiguous argument 'HEAD': unknown revision or path not in the working tree.
    git rev-parse --abbrev-ref --verify --quiet HEAD
}
function gitLastCommit()
{
    # do not display error when there is no commit yet
    git log -1 --oneline 2>>/dev/null
}

# custom PS1
# \u user, \h hostname, \w current directory, \$ user status
# do not use raw escapes, use tput
#  colors and styles: \033[00m where 00 is the default color
#  text colors: 30 to 37, background colors: 40 to 47, bold 01, underlined 04, blink 05, line-through 07
#  put each \033[XXm inside brackets to avoid display bugs: \[\033[00m\]
reset=$(tput sgr0)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)
purple=$(tput setaf 5)
bluegreen=$(tput setaf 6)
white=$(tput setaf 7)
invisible=$(tput setaf 8)
orange=$(tput setaf 9)
boldOn=$(tput bold)
boldOff=$(tput sgr0)
# put a \ at the end of each line to write the new PS1 value on several lines, for lisibility
# the git command displays ✔ if the current dir is a git repository;
# else displays the path of the nearest parent being one
# else if no git at all, shows nothing (throws git error message into /dev/null)
# didier@chaotic-arch:~ [Git: ✔]
# └─5026─10:55:56─$
PS1='\
\[$green\]\[$boldOn\]\u\[$boldOff\]\[$blue\]@\h:\[$yellow\]\w \[$purple\]$(gitIsGitRepository)\n\
\[$green\]└─\!─\[$bluegreen\]\t\[$green\]─\$\[$reset\] \
'

# My passwords needed by some aliases in .bash_aliases.
if [ -f ~/.bash_passwords ]; then
    . ~/.bash_passwords
fi
# Alias definitions.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# When bash is started non-interactively, for example, to run a shell script,
# it looks for the variable BASH_ENV in the environment, expands its value if it appears there,
# and uses the expanded value as the name of a file to read and execute.
export BASH_ENV=~/.bashrc

# use most as default pager (must be installed)
export PAGER="most -s"

export EDITOR="vim"

# Normally, apps are built through the Android Studio GUI. To build apps from the commandline (using e.g. ./gradle assembleDebug)
export ANDROID_HOME=/opt/android-sdk
# fix for missing radeon driver
# https://stackoverflow.com/a/39575542
export ANDROID_EMULATOR_USE_SYSTEM_LIBS=1

PATH=/usr/share/php:/usr/share/pear/bin:/home/didier/bin/:/home/didier/.gem/ruby/2.3.0/bin/:/home/didier/.config/composer/vendor/bin:$PATH

# trying to fix Skype core dump
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/i386-linux-gnu

# As any other environment variable you set in .bashrc or .bash_profile, the PATH variable is not available to systemd. If you customize your PATH and plan on launching applications that make use of it from systemd units, you should make sure the modified PATH is set on the systemd environment
# https://wiki.archlinux.org/index.php/Systemd/User#PATH
systemctl --user import-environment PATH
# Required by ~/.config/systemd/user/feh-wallpaper.service
systemctl --user import-environment DISPLAY
# $ systemctl --user enable feh-wallpaper.service
# $ systemctl --user enable feh-wallpaper.timer

export FRAMEBUFFER=/dev/fb0
alias fbterm='FBTERM=1 fbterm'
[ -n "$FBTERM" ] && export TERM=fbterm
alias tmux='TMUX=1 tmux'
[ -n "$TMUX" ] && export TERM=screen-256color

# Make Java apps use anti-aliased fonts and GTK look and feel
# Copy this line in /etc/profile.d/jre.sh to affect programs that are not run by sourcing ~/.bashrc
# https://wiki.archlinux.org/index.php/Java#Tips_and_tricks
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'

# https://askubuntu.com/questions/813588/systemctl-failed-to-connect-to-bus-docker-ubuntu16-04-container
export XDG_RUNTIME_DIR=/run/user/`id -u`

# Enable grunt auto-completion. This assumes that Grunt has been installed
# globally with npm install -g grunt. Currently, the only supported shell is bash.
eval "$(grunt --completion=bash)"

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi
export LS_COLORS="di=00;34:ow=01;44:ex=00;31"

###########
# ALIASES #
###########
# cp always recursive and verbose
alias cp='cp -rv'
alias rm='rm --interactive --verbose'
alias mv='mv --interactive --verbose'
alias mkdir='mkdir -p'
# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
# ls display all with details, human-readable and coloration
alias cls='ls -alh'
alias dd='dd status=progress'
alias psc='ps xawf -eo pid,user,cgroup,args'
alias rsync='rsync -h --stats --progress' #human readable
# Add an "alert" alias for long running commands. Dependency:
# $ sudo pacman -S libnotify
# Usage:
# $ sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
alias trash='trash-put'
# Show dirs that do not belong to any package
alias pacman-disowned-dirs="comm -23 <(sudo find / \( -path '/dev' -o -path '/sys' -o -path '/run' -o -path '/tmp' -o -path '/mnt' -o -path '/srv' -o -path '/proc' -o -path '/boot' -o -path '/home' -o -path '/root' -o -path '/media' -o -path '/var/lib/pacman' -o -path '/var/cache/pacman' \) -prune -o -type d -print | sed 's/\([^/]\)$/\1\//' | sort -u) <(pacman -Qlq | sort -u)"
# Show files that do not belong to any package:
alias pacman-disowned-files="comm -23 <(sudo find / \( -path '/dev' -o -path '/sys' -o -path '/run' -o -path '/tmp' -o -path '/mnt' -o -path '/srv' -o -path '/proc' -o -path '/boot' -o -path '/home' -o -path '/root' -o -path '/media' -o -path '/var/lib/pacman' -o -path '/var/cache/pacman' \) -prune -o -type f -print | sort -u) <(pacman -Qlq | sort -u)"
